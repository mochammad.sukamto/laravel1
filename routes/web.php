<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('welcome');
});

// Route::get('/index', function () {
//     return view('index');
// });

// Route::get('/buataccount', function () {
//     return view('buataccount');
// });

// Route::get('/welcome1', function () {
//     return view('welcome1');
// });

Route::get('/test/{angka}', function ($angka) {
    return view('test', ["angka" => $angka]);
});

Route::get('/halo/{nama}', function ($nama) {
    return "HALO $nama";
});

Route::get('/form', 'RegisterController@form');

Route::get('/sapa', 'RegisterController@sapa');

Route::get('/', 'HomeController@index');

Route::get('/master', function() {
    return view ('adminlte.master');
});

Route::get('/items', function() {
    return view ('items.index1');
});
Route::get('/items/create', function(){
    return view ('items.create');
});

// Route::get('/register', 'AuthController@register');
// Route::get('/welcome', 'AuthController@welcome');
// Route::get('/submit/{nama}', function ($nama) {
//     return "$nama";
// });

Route::get('/register', ['as'=>'/register','uses'=>'AuthController@register']);
Route::post('/welcome', ['as'=>'/welcome','uses'=>'AuthController@welcome']);

Route::get('/items/table', function() {
    return view ('items.table');
});
Route::get('/items/datatables', function() {
    return view ('items.datatables');
});